(function() {
    'use strict';
    $(document).ready(function() {
        $('.about').click(function() {
            $('.aboutContent').show();
            $('.experienceContent').hide();
            $('.skillContent').hide();
        });
        $('.experience').click(function() {
            $('.aboutContent').hide();
            $('.experienceContent').show();
            $('.skillContent').hide();
        });
        $('.skills').click(function() {
            $('.aboutContent').hide();
            $('.experienceContent').hide();
            $('.skillContent').show();
        });
    });
}());


//# sourceMappingURL=dist.app.js.map
